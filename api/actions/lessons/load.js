import {getDbConnection} from '../../utils/cradle';
import {parseUrl} from '../../utils/url';

export default function load(req) {
  return new Promise((resolve, reject) => {
    const params = parseUrl(req.url);

    if (params.length === 2) {
      getDbConnection().all(
          {include_docs: true},
          (err, docs) => {
            if (err) {
              reject(err);
            } else {
              resolve(docs);
            }
          }
      );
    } else {
      getDbConnection().get(
          params[2],
          (err, docs) => {
            if (err) {
              reject(err);
            } else {
              resolve(docs);
            }
          }
      );
    }
  });
}
