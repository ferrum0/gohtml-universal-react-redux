/**
 * Created by e on 4/22/17.
 */

import cradle from 'cradle';

const dbConfig = {
    name: 'lessons',
    host: '127.0.0.1',
    port: 5984,
    couchConf: {
        cache: true,
        raw: false,
        forceSave: true,
        auth: {
            username: 'god',
            password: 'RjnZorro42'
        }
    }
};

/**
 * @return cradle.Database
 */
export function getDbConnection(){
    return (
        new (cradle.Connection)(
            dbConfig.host,
            dbConfig.port,
            dbConfig.couchConf
        ).database(dbConfig.name)
    );
}

