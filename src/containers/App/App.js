import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {LinkContainer} from 'react-router-bootstrap';
import NavItem from 'react-bootstrap/lib/NavItem';
import {isLoaded as isInfoLoaded, load as loadInfo} from 'redux/modules/info';
import {isLoaded as isAuthLoaded, load as loadAuth, logout} from 'redux/modules/auth';
import {InfoBar} from 'components';
import {push} from 'react-router-redux';
import {asyncConnect} from 'redux-async-connect';
import Helmet from 'react-helmet';
import config from '../../config';

@asyncConnect([{
  promise: ({store: {dispatch, getState}}) => {
    const promises = [];

    if (!isInfoLoaded(getState())) {
      promises.push(dispatch(loadInfo()));
    }
    if (!isAuthLoaded(getState())) {
      promises.push(dispatch(loadAuth()));
    }

    return Promise.all(promises);
  }
}])
@connect(
    state => ({user: state.auth.user}),
    {logout, pushState: push})
export default class App extends Component {
  static propTypes = {
    children: PropTypes.object.isRequired,
    user: PropTypes.object,
    logout: PropTypes.func.isRequired,
    pushState: PropTypes.func.isRequired
  };

  static contextTypes = {
    store: PropTypes.object.isRequired
  };

  componentWillReceiveProps(nextProps) {
    if (!this.props.user && nextProps.user) {
      // login
      this.props.pushState('/loginSuccess');
    } else if (this.props.user && !nextProps.user) {
      // logout
      this.props.pushState('/');
    }
  }

  handleLogout = (event) => {
    event.preventDefault();
    this.props.logout();
  };

  render() {
    const {user} = this.props;
    const styles = require('./App.scss');

    return (
        <div className={styles.app}>
          <Helmet {...config.app.head}/>

          <ul className={styles.headNav}>
            {user && <LinkContainer to="/chat">
              <NavItem eventKey={1}>Chat</NavItem>
            </LinkContainer>}

            <LinkContainer to="/widgets">
              <NavItem eventKey={2}>Widgets</NavItem>
            </LinkContainer>
            <LinkContainer to="/lessons">
              <NavItem eventKey={8}>Lessons</NavItem>
            </LinkContainer>
            <LinkContainer to="/survey">
              <NavItem eventKey={3}>Survey</NavItem>
            </LinkContainer>
            <LinkContainer to="/pagination">
              <NavItem eventKey={4}>Pagination</NavItem>
            </LinkContainer>
            <LinkContainer to="/about">
              <NavItem eventKey={5}>About Us</NavItem>
            </LinkContainer>

            {!user &&
            <LinkContainer to="/login">
              <NavItem eventKey={6}>Login</NavItem>
            </LinkContainer>}
            {user &&
            <LinkContainer to="/logout">
              <NavItem eventKey={7} className="logout-link" onClick={this.handleLogout}>
                Logout
              </NavItem>
            </LinkContainer>}
          </ul>

          <div className={styles.appContent}>
            {this.props.children}
          </div>
          <InfoBar/>

          <div className="well text-center">
            Have questions? Ask for help <a
              href="https://github.com/erikras/react-redux-universal-hot-example/issues"
              target="_blank">on Github</a> or in the <a
              href="https://discord.gg/0ZcbPKXt5bZZb1Ko" target="_blank">#react-redux-universal</a> Discord channel.
          </div>
        </div>
    );
  }
}
