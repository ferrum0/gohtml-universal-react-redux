import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import * as lessonActions from 'redux/modules/lessons';
import {isLoaded, load as loadLessons} from 'redux/modules/lessons';
import {initializeWithKey} from 'redux-form';
import {asyncConnect} from 'redux-async-connect';
import {LinkContainer} from 'react-router-bootstrap';

@asyncConnect([{
  deferred: true,
  promise: ({store: {dispatch, getState}}) => {
    if (!isLoaded(getState())) {
      return dispatch(loadLessons());
    }
  }
}])

@connect(
  state => ({
    lessons: state.lessons.data,
    editing: state.lessons.editing,
    error: state.lessons.error,
    loading: state.lessons.loading
  }),
  {...lessonActions, initializeWithKey}
)

export default class Lessons extends Component {
  static propTypes = {
    lessons: PropTypes.array,
    error: PropTypes.object,
    loading: PropTypes.bool,
    initializeWithKey: PropTypes.func.isRequired,
    editing: PropTypes.object.isRequired,
    load: PropTypes.func.isRequired,
    editStart: PropTypes.func.isRequired
  };

  render() {
    const {lessons, error, loading, load} = this.props;

    let refreshClassName = 'fa fa-refresh';
    if (loading) {
      refreshClassName += ' fa-spin';
    }
    const styles = require('./Lessons.scss');
    return (
        <div className={styles.lessons + ' container'}>
          <h1>
            Lessons
            <button className={styles.refreshBtn + ' btn btn-success'} onClick={load}>
              <i className={refreshClassName}/> {' '} Reload Lessons
            </button>
          </h1>
          {error &&
          <div className="alert alert-danger" role="alert">
            <span className="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            {' '}
            {error}
          </div>}
          {lessons && lessons.length &&
          lessons.map(
              (rowData) => {
                const lesson = rowData.doc;

                return (
                    <div className={styles.listItem + ' col-md-6'}>
                      <LinkContainer to={'/lessons/' + lesson._id}>
                        <div className={styles.avatar}
                            style={{backgroundImage: 'url(' + lesson.avatar + ')'}}>
                          <div className={styles.title}>{lesson.title}</div>
                          <div className={styles.shortText}>{lesson.shortText}</div>
                        </div>
                      </LinkContainer>
                    </div>
                );
              }
          )}
        </div>
    );
  }
}

