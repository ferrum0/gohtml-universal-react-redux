import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import * as lessonActions from 'redux/modules/view-lesson';
import {initializeWithKey} from 'redux-form';
import {isLoaded, load as loadLesson} from 'redux/modules/view-lesson';
import {asyncConnect} from 'redux-async-connect';

@asyncConnect([{
  deferred: true,
  promise: ({store: {dispatch, getState}, params: {id}}) => {
    if (!isLoaded(getState())) {
      return dispatch(loadLesson(id));
    }
  }
}])

@connect(
  state => ({
    widgets: state.viewLesson.data,
    editing: state.viewLesson.editing,
    error: state.viewLesson.error,
    loading: state.viewLesson.loading
  }),
  {...lessonActions, initializeWithKey}
)

export default class ViewLesson extends Component {
  static propTypes = {
    widgets: PropTypes.array,
    error: PropTypes.object,
    loading: PropTypes.bool,
    params: PropTypes.object,
    initializeWithKey: PropTypes.func.isRequired,
    editing: PropTypes.object.isRequired,
    load: PropTypes.func.isRequired,
    editStart: PropTypes.func.isRequired
  };

  render() {
    const {widgets, error, loading, load} = this.props;
    let refreshClassName = 'fa fa-refresh';
    if (loading) {
      refreshClassName += ' fa-spin';
    }
    const styles = require('./ViewLesson.scss');
    return (
        <div className={styles.lessons + ' container'}>
          {error &&
          <div className="alert alert-danger" role="alert">
            <span className="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            {' '}
            {error}
          </div>}
          {widgets && (
              (() => {
                const lesson = widgets;

                return (
                    <div className={styles.listItem}>
                      <div className={styles.text}>
                        <div className={styles.avatarContainer}>
                          <div className="row">
                            <div className={styles.title}>{lesson.title}</div>
                          </div>
                          <div className="row">
                            <div className={styles.avatar}
                                 style={{backgroundImage: 'url(' + lesson.avatar + ')'}}>
                              <button className={styles.refreshBtn + ' btn btn-success'}
                                      onClick={load.bind(null, this.props.params.id)}>
                                <i className={refreshClassName}/>
                              </button>
                            </div>
                          </div>
                        </div>

                        {lesson.text}
                      </div>
                    </div>
                );
              })()
          )}
        </div>
    );
  }
}

